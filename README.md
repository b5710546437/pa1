# Stopwatch by Arut Thanomwatana (5710546437) #

I ran the tasks on a Windows 8.1 with WMC  64-bit, and got these
results:

## The times reported for running each task and explain the differences ##

* Why is there such a big difference in the time used to append chars to a String and to a StringBuilder?
* Why is there a significant difference in times to sum double, Double, and BigDecimal values?

## A big difference in the time used to append chars to a String and to a StringBuilder. ##

```sh
                  Task                 |     Time
-------------------------------------- | ------------
Append 100,000 char to String          | 5.090675 sec
Append 100,000 char to StringBuilder   | 0.003748 sec
```
### Explaination of result ###
Appending StringBuilder is faster than String because StringBuilder don't have thread safe.

## A significant difference in times to sum double, Double, and BigDecimal values. ##

```sh
                   Task                       |     Time
--------------------------------------------- | ------------
Sum 100,000,000 array of double primitives    | 0.180874 sec
Sum 100,000,000 array of Double objects       | 0.883472 sec
Sum 100,000,000 array of BigDecimal           | 1.364511 sec
```
### Explaination of result ###
BigDecimal is slowest because It's most precision.Double object is slower than double primitive because Double object have to create object first,On the other hand java has double primitive already so we don't need to create it.