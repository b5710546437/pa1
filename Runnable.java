/**
 * 
 * An Interface used for 5 task
 * @author Arut Thanomwatana
 *
 */
public interface Runnable {
	
	public void run();
	public String getResult();
	public String toString();
}
