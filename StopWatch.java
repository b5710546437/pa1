/**
 * A Stopwatch that measure elasped time between starting time until stop time or until present
 * @author Arut Thanomwatana
 *
 */

public class StopWatch 
{
	private long start;
	private long end;
	private long elapsedTime;
	private boolean checkRun;
	/**
	 * Create and Initialize Stopwatch
	 * 
	 */
	public StopWatch()
	{
		start = 0;
		end = 0;
		checkRun = false;

	}
	/**
	 * Get Elasped time from start to end or to present time.
	 * @return elaspedTime from start to end or to present time.
	 */
	public double getElapsed()
	{
		if(isRunning())
		{
			elapsedTime = System.nanoTime()-start;
		}
		else
		{
			elapsedTime = end - start;
		}
		return elapsedTime*Math.pow(10, -9);
	}
	/**
	 * Check if Stopwatch is running or not
	 * @return true if stopwatch is running.
	 */
	public boolean isRunning()
	{
		return checkRun;
	}
	/**
	 * Start the stopwatch.
	 */
	public void start()
	{
		if(!isRunning())
		{
			start = System.nanoTime();
			checkRun= true;
		}
	}
	/**
	 * Stop the stopwatch.
	 */
	public void stop()
	{
		if(isRunning())
		{
			end = System.nanoTime();
			checkRun = false;
		}
	}
}
