
public class TaskTimer
{

/**
 * Measure the time that used to execute each task
 * @param runnable is a task
 */
	
	public static void measureAndPrint(Runnable runnable)
	{
		StopWatch timer = new StopWatch();
		System.out.printf(runnable.toString());
		timer.start();
		runnable.run();
		System.out.println(runnable.getResult());
		timer.stop();
		System.out.printf("Elapsed time %.6f sec\n\n", timer.getElapsed());

	}


}
