/**
 * Task 4 : TestSumDouble
 * @author Arut Thanomwatana
 *
 */
public class TestSumDouble implements Runnable 
{
	private static final int ARRAY_SIZE = 500000;
	private static final int counter = 100000000;
	private Double[] values;
	private Double sum;

	/**
	 * Create and Initialize Task 4
	 */
	public TestSumDouble()
	{
		values = new Double[ARRAY_SIZE];
		for(int i=0; i<ARRAY_SIZE; i++) values[i] = new Double(i+1);

	}
	/**
	 * Run Task 4
	 */
	public void run()
	{
		sum = new Double(0.0);
		for(int count=0, i=0; count<counter; count++, i++) {
			if (i >= ARRAY_SIZE) i = 0;
			sum = sum + values[i];
		}
	}
	/**
	 * Define Task 4
	 * @return String that define Task 4
	 */
	public String toString()
	{
		return String.format("Sum array of Double objects with count=%,d\n", counter);
	}
	/**
	 * Get the result from Task 4
	 * @return result from Task 4
	 */
	public String getResult()
	{
		return String.format("sum = "+ sum+"\n");
	}

}
