import java.math.BigDecimal;

/**
 * Task 5 : TestSumBigDecimal
 * @author Arut Thanomwatana
 *
 */
public class TestSumBigDecimal implements Runnable
{
	private static final int counter = 100000000;
	private BigDecimal[] values;
	private static final int ARRAY_SIZE = 500000;
	private BigDecimal sum;
	/**
	 * Create and Initialize Task 5
	 */
	public TestSumBigDecimal()
	{
		values = new  BigDecimal[ARRAY_SIZE];
		for(int i=0; i<ARRAY_SIZE; i++) values[i] = new BigDecimal(i+1);
	}

	/**
	 * Run Task 5
	 */
	public void run()
	{
		sum = new BigDecimal(0.0);
		for(int count=0, i=0; count<counter; count++, i++) {
			if (i >= ARRAY_SIZE) i = 0;
			sum = sum.add( values[i] );
		}
	}

	/**
	 * Define Task 5
	 * @return String taht define task 5
	 */
	public String toString()
	{

		return String.format("Sum array of BigDecimal with count=%,d\n", counter);

	}
	/**
	 * Get the result from task 5
	 * @return result from Task 5
	 */
	public String getResult()
	{
		return String.format("sum = "+ sum +"\n");
	}

}
