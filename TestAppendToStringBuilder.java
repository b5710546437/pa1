/**
 * Task 2 : TestAppendToStringBuilder
 * @author Arut Thanomwatana
 *
 */
public class TestAppendToStringBuilder implements Runnable
{
	final int counter = 100000;
	final char CHAR = 'a';
	private String result;
	/**
	 * Create and Initialize Task 2
	 */
	public TestAppendToStringBuilder()
	{
		result = "";

	}
	/**
	 * Run Task 2
	 */
	public void run()
	{
		StringBuilder builder = new StringBuilder(); 
		int k = 0;
		while(k++ < counter) {
			builder = builder.append(CHAR);
		}
		result = builder.toString();
	}
	/**
	 * Define Task 2
	 * @return String that refine task 2
	 */
	public String toString()
	{

		return String.format("Append to StringBuilder with count=%,d\n", counter);
	}
	/**
	 * Get the result from Task 2
	 * @return result from task 2
	 */
	public String getResult()
	{
		return String.format("final string length = " + result.length()+"\n");
	}

}
