/**
 * Task 1 : TestAppendToString
 * @author Arut Thanomwatana
 *
 */
public class TestAppendToString implements Runnable
{
	
	final char CHAR = 'a';
	final int counter = 100000;
	private String sum;

	/**
	 * Create and Initialize Task1
	 */
	public TestAppendToString()
	{
		sum = "";
	}
	/**
	 * Run task 1
	 */
	public void run()
	{
		int k =0;		
		while(k++ < counter) {
			sum = sum + CHAR;
		}
	}
	/**
	 * Define Task 1
	 * @return String that define task 1
	 */
	public String toString()
	{
		return String.format("Append to String with count = %d\n",counter);		
	}
	/**
	 * Get Result from Task 1
	 * @return result from Task1
	 */
	public String getResult()
	{
		return String.format("final string length = %d\n",this.sum.length());
	}
	

}
