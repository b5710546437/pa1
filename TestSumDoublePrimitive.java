/**
 * Task 3 : TestSumDoublePrimitive
 * @author Arut Thanomwatana
 *
 */
public class TestSumDoublePrimitive implements Runnable
{
	private static final int ARRAY_SIZE = 500000;
	private static final int counter = 100000000;
	private double[] values;
	private double sum;
	/**
	 * Create and Initialize Task 3
	 */
	public TestSumDoublePrimitive()
	{
		values = new double[ARRAY_SIZE];
		for(int k=0; k<ARRAY_SIZE; k++) values[k] = k+1;

	}
	/**
	 * Run Task 3
	 */
	public void run()
	{
		sum = 0.0;
		for(int count=0, i=0; count<counter; count++, i++) {
			if (i >= ARRAY_SIZE) i = 0;
			sum = sum + values[i];
		}
	}
	/**
	 * Define Task 3
	 * @return String that define task 3
	 */
	public String toString()
	{

		return String.format("Sum array of double primitives with count=%,d\n", counter);
	}
	/**
	 * Get the result from Task 3
	 * @return result from task 3
	 */
	public String getResult()
	{
		return String.format("sum = " + sum + "\n");
	}

}
